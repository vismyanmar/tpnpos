package dto;

public class Shelf {
	private int shelfId;
	private String shelfCode;
	private String shelfRemarks;

	public Shelf(String shelf_Code, String shelf_Remarks) {
		super();
		shelfCode = shelf_Code;
		shelfRemarks = shelf_Remarks;
	}

	public int getShelf_Id() {
		return shelfId;
	}

	public void setShelf_Id(int shelf_Id) {
		shelfId = shelf_Id;
	}

	public String getShelf_Code() {
		return shelfCode;
	}

	public void setShelf_Code(String shelf_Code) {
		shelfCode = shelf_Code;
	}

	public String getShelf_Remarks() {
		return shelfRemarks;
	}

	public void setShelf_Remarks(String shelf_Remarks) {
		shelfRemarks = shelf_Remarks;
	}

}
