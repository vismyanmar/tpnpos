package dto;

public class Item {
	private int itemId;
	private String itemCode;
	private String itemName;
	private String Description;
	private double pricePurchased;
	private double priceSelling;
	private double priceCustomer;
	private int Quantity;
	private int customizedQuantity;
	private int Minimum;

	public Item(String item_Code, String item_Name, String description, double price_Purchased, double price_Selling,
			double price_Customer, int quantity, int customized_Quantity, int minimum) {
		super();
		itemCode = item_Code;
		itemName = item_Name;
		Description = description;
		pricePurchased = price_Purchased;
		priceSelling = price_Selling;
		priceCustomer = price_Customer;
		Quantity = quantity;
		customizedQuantity = customized_Quantity;
		Minimum = minimum;
	}

	public int getItem_Id() {
		return itemId;
	}

	public void setItem_Id(int item_Id) {
		itemId = item_Id;
	}

	public String getItem_Code() {
		return itemCode;
	}

	public void setItem_Code(String item_Code) {
		itemCode = item_Code;
	}

	public String getItem_Name() {
		return itemName;
	}

	public void setItem_Name(String item_Name) {
		itemName = item_Name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public double getPrice_Purchased() {
		return pricePurchased;
	}

	public void setPrice_Purchased(double price_Purchased) {
		pricePurchased = price_Purchased;
	}

	public double getPrice_Selling() {
		return priceSelling;
	}

	public void setPrice_Selling(double price_Selling) {
		priceSelling = price_Selling;
	}

	public double getPrice_Customer() {
		return priceCustomer;
	}

	public void setPrice_Customer(double price_Customer) {
		priceCustomer = price_Customer;
	}

	public int getQuantity() {
		return Quantity;
	}

	public void setQuantity(int quantity) {
		Quantity = quantity;
	}

	public int getCustomized_Quantity() {
		return customizedQuantity;
	}

	public void setCustomized_Quantity(int customized_Quantity) {
		customizedQuantity = customized_Quantity;
	}

	public int getMinimum() {
		return Minimum;
	}

	public void setMinimum(int minimum) {
		Minimum = minimum;
	}

}
