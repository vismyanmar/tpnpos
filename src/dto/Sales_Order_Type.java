package dto;

public class Sales_Order_Type {

	private int salesOrderTypeId;
	private String salesOrderType;

	public Sales_Order_Type(String sales_Order_Type) {
		super();
		salesOrderType = sales_Order_Type;
	}

	public int getSales_Order_Type_Id() {
		return salesOrderTypeId;
	}

	public void setSales_Order_Type_Id(int sales_Order_Type_Id) {
		salesOrderTypeId = sales_Order_Type_Id;
	}

	public String getSales_Order_Type() {
		return salesOrderType;
	}

	public void setSales_Order_Type(String sales_Order_Type) {
		salesOrderType = sales_Order_Type;
	}
	

}
