package dto;

public class Country {
	private int countryId;
	private String countryCode;
	private String countryName;

	public Country(String country_Code, String country_Name) {
		super();
		countryCode = country_Code;
		countryName = country_Name;
	}

	public int getCountry_Id() {
		return countryId;
	}

	public void setCountry_Id(int country_Id) {
		countryId = country_Id;
	}

	public String getCountry_Code() {
		return countryCode;
	}

	public void setCountry_Code(String country_Code) {
		countryCode = country_Code;
	}

	public String getCountry_Name() {
		return countryName;
	}

	public void setCountry_Name(String country_Name) {
		countryName = country_Name;
	}

}
