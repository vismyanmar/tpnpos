package dto;

public class Unit {
	private int unitId;
	private String unitCode;
	private String unitName;
	private String unitRemark;

	public Unit(String unit_Code, String unit_Name, String unit_Remark) {
		super();
		unitCode = unit_Code;
		unitName = unit_Name;
		unitRemark = unit_Remark;
	}

	public int getUnit_Id() {
		return unitId;
	}

	public void setUnit_Id(int unit_Id) {
		unitId = unit_Id;
	}

	public String getUnit_Code() {
		return unitCode;
	}

	public void setUnit_Code(String unit_Code) {
		unitCode = unit_Code;
	}

	public String getUnit_Name() {
		return unitName;
	}

	public void setUnit_Name(String unit_Name) {
		unitName = unit_Name;
	}

	public String getUnit_Remark() {
		return unitRemark;
	}

	public void setUnit_Remark(String unit_Remark) {
		unitRemark = unit_Remark;
	}

}
