package dto;

public class Warehouse {
	private int warehouseId;
	private String warehouseName;
	private String warehouseRemarks;

	public Warehouse(String warehouse_Name, String warehouse_Remarks) {
		super();
		warehouseName = warehouse_Name;
		warehouseRemarks = warehouse_Remarks;
	}

	public int getWarehouse_Id() {
		return warehouseId;
	}

	public void setWarehouse_Id(int warehouse_Id) {
		warehouseId = warehouse_Id;
	}

	public String getWarehouse_Name() {
		return warehouseName;
	}

	public void setWarehouse_Name(String warehouse_Name) {
		warehouseName = warehouse_Name;
	}

	public String getWarehouse_Remarks() {
		return warehouseRemarks;
	}

	public void setWarehouse_Remarks(String warehouse_Remarks) {
		warehouseRemarks = warehouse_Remarks;
	}

}
