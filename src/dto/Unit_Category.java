package dto;

public class Unit_Category {
	private int unitCategoryId;
	private String unitCategoryName;
	private String unitCategoryRemarks;

	public Unit_Category(String unit_CategoryName, String unit_Category_Remarks) {
		super();
		unitCategoryName = unit_CategoryName;
		unitCategoryRemarks = unit_Category_Remarks;
	}

	public int getUnit_Category_Id() {
		return unitCategoryId;
	}

	public void setUnit_Category_Id(int unit_Category_Id) {
		unitCategoryId = unit_Category_Id;
	}

	public String getUnit_CategoryName() {
		return unitCategoryName;
	}

	public void setUnit_CategoryName(String unit_CategoryName) {
		unitCategoryName = unit_CategoryName;
	}

	public String getUnit_Category_Remarks() {
		return unitCategoryRemarks;
	}

	public void setUnit_Category_Remarks(String unit_Category_Remarks) {
		unitCategoryRemarks = unit_Category_Remarks;
	}

}
