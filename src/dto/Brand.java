package dto;

public class Brand {
	private int brandId;
	private String brandName;
	private String brandRemarks;

	public Brand(String brand_Name, String brand_Remarks) {
		super();
		brandName = brand_Name;
		brandRemarks = brand_Remarks;
	}

	public int getBrand_Id() {
		return brandId;
	}

	public void setBrand_Id(int brand_Id) {
		brandId = brand_Id;
	}

	public String getBrand_Name() {
		return brandName;
	}

	public void setBrand_Name(String brand_Name) {
		brandName = brand_Name;
	}

	public String getBrand_Remarks() {
		return brandRemarks;
	}

	public void setBrand_Remarks(String brand_Remarks) {
		brandRemarks = brand_Remarks;
	}

}


