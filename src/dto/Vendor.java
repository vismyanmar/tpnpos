package dto;

import java.sql.Blob;

public class Vendor {
	private int vendorId;
	private String vendorName;
	private String vendorAddress;
	private String vendorPhone;
	private Blob vendorPhoto;

	public Vendor(String vendor_Name, String vendor_Address, String vendor_Phone, Blob vendor_Photo) {
		super();
		vendorName = vendor_Name;
		vendorAddress = vendor_Address;
		vendorPhone = vendor_Phone;
		vendorPhoto = vendor_Photo;
	}

	public int getVendor_Id() {
		return vendorId;
	}

	public void setVendor_Id(int vendor_Id) {
		vendorId = vendor_Id;
	}

	public String getVendor_Name() {
		return vendorName;
	}

	public void setVendor_Name(String vendor_Name) {
		vendorName = vendor_Name;
	}

	public String getVendor_Address() {
		return vendorAddress;
	}

	public void setVendor_Address(String vendor_Address) {
		vendorAddress = vendor_Address;
	}

	public String getVendor_Phone() {
		return vendorPhone;
	}

	public void setVendor_Phone(String vendor_Phone) {
		vendorPhone = vendor_Phone;
	}

	public Blob getVendor_Photo() {
		return vendorPhoto;
	}

	public void setVendor_Photo(Blob vendor_Photo) {
		vendorPhoto = vendor_Photo;
	}

}
