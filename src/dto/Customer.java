package dto;

public class Customer {
	private int customerId;
	private String customerName;
	private String customerAddress;
	private String customerPhone;

	public Customer(String customer_Name, String customer_Address, String customer_Phone) {
		super();
		customerName = customer_Name;
		customerAddress = customer_Address;
		customerPhone = customer_Phone;
	}

	public int getCustomer_Id() {
		return customerId;
	}

	public void setCustomer_Id(int customer_Id) {
		customerId = customer_Id;
	}

	public String getCustomer_Name() {
		return customerName;
	}

	public void setCustomer_Name(String customer_Name) {
		customerName = customer_Name;
	}

	public String getCustomer_Address() {
		return customerAddress;
	}

	public void setCustomer_Address(String customer_Address) {
		customerAddress = customer_Address;
	}

	public String getCustomer_Phone() {
		return customerPhone;
	}

	public void setCustomer_Phone(String customer_Phone) {
		customerPhone = customer_Phone;
	}

}
