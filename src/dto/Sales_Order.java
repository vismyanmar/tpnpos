package dto;

import java.util.Date;

public class Sales_Order {
	private int salesOrderId;
	private String invoiceCode;
	private Date salesOrderDateTime;
	private double totalPrice;
	private double leftToBePaid;
	private boolean paidStatus;

	public Sales_Order(String invoice_Code, Date sales_Order_Date_Time, double total_Price, double left_To_Be_Paid,
			boolean paid_Status) {
		super();
		invoiceCode = invoice_Code;
		salesOrderDateTime = sales_Order_Date_Time;
		totalPrice = total_Price;
		leftToBePaid = left_To_Be_Paid;
		paidStatus = paid_Status;
	}

	public int getSales_Order_Id() {
		return salesOrderId;
	}

	public void setSales_Order_Id(int sales_Order_Id) {
		salesOrderId = sales_Order_Id;
	}

	public String getInvoice_Code() {
		return invoiceCode;
	}

	public void setInvoice_Code(String invoice_Code) {
		invoiceCode = invoice_Code;
	}

	public Date getSales_Order_Date_Time() {
		return salesOrderDateTime;
	}

	public void setSales_Order_Date_Time(Date sales_Order_Date_Time) {
		salesOrderDateTime = sales_Order_Date_Time;
	}

	public double getTotal_Price() {
		return totalPrice;
	}

	public void setTotal_Price(double total_Price) {
		totalPrice = total_Price;
	}

	public double getLeft_To_Be_Paid() {
		return leftToBePaid;
	}

	public void setLeft_To_Be_Paid(double left_To_Be_Paid) {
		leftToBePaid = left_To_Be_Paid;
	}

	public boolean isPaid_Status() {
		return paidStatus;
	}

	public void setPaid_Status(boolean paid_Status) {
		paidStatus = paid_Status;
	}

}
